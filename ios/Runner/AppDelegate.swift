import UIKit
import Flutter
import AudioToolbox
import AVFoundation

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    
    GeneratedPluginRegistrant.register(with: self)
    do  {
        print("Setting AudioSestion")
        try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
    } catch {
        //TODO
        print("Error while setting audio managerSession")
    }
    // This will enable to show nowplaying controls on lock screen
    application.beginReceivingRemoteControlEvents()
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
