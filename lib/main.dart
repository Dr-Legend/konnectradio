import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:konnectradio/src/utils/kradioApp.dart';
import 'package:konnectradio/src/widgets/kradio_player.dart';
import 'package:flutter/foundation.dart';

RmxAudioPlayer rmxAudioPlayer = new RmxAudioPlayer();

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'konnectRadio',
      debugShowCheckedModeBanner: false,
      home: KonnectRadioPlayer(
        rmxAudioPlayer: rmxAudioPlayer,
      ),
    );
  }
}
