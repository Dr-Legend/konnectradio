import 'package:flutter/material.dart';

enum PlayerStates { stopped, playing, paused }

class KonnectRadioConstant {
  static String facebookUrl = "https://www.facebook.com/konnectradio";
  static String twitterUrl = "https://twitter.com/konnectradio";
  static String instagramUrl = "https://www.instagram.com/konnectradio";
  static String emailUrl = "mailto:studio@konnectradio.com";
//  static String listenStream =
//      "http://rss-streaming.co.uk/proxy/konnectradio?mp=/stream";
//  static String listenStream =
//      "http://217.174.240.195/proxy/konnectradio?mp=/stream";
  static String listenStream = "http://217.174.240.195:8109";
  static String listenStream1 =
      "https://securestreams3.autopo.st:1370/proxy/konnectradio?mp=/stream";
  static String streamInfoAPi =
      "https://rss-streaming.co.uk:3389/rpc/konnectradio/streaminfo.get";
  static String recentStreams =
      "https://rss-streaming.co.uk:3389/recentfeed/konnectradio/json/";
  static getTrackCoverImageURL({@required String query}) {
    return "https://itunes.apple.com/search?term=$query&media=music&limit=1";
  }
}

class KonnectRadioColors {
  static Color purple = const Color(0xFF5F47B2);
  static Color lightGreen = const Color(0xFF00FF99);
  static Color green = const Color(0xFF00FF11);
  static Color lightNavyBlue = const Color(0xFFADB9CD);
  static Color navyBlue = const Color(0xFF4D6B9C);
  static Color lightPurple = const Color(0xFF9F91D1);
}
