import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

///This class has API calling methods
///
class KonnectRadioApi {
  ///This method handles request in GET
  Future<String> getStreamInfo(String url) async {
    try {
      HttpClient client = new HttpClient();
      client.badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);

      HttpClientRequest request = await client.getUrl(Uri.parse(url));
      request.headers.set('content-type', 'application/json');

      HttpClientResponse response = await request.close();

      String reply = await response.transform(utf8.decoder).join();

//    final response =
//        await http.get(url, headers: {"Accept": "application/json"});
//    print(response.body);
      if (response.statusCode == 200) {
        /// If server returns an OK response, return the response
//      return response.body;
        return reply;
      } else {
        throw Exception('Failed to load data');
      }
    } catch (t, e) {
      print(e);
    }
  }

  Future<String> getTrackImage(String url) async {
    try {
      final response = await http.post(url);
      if (response.statusCode == 200) {
        /// If server returns an OK response, return the response
        String coverImageUrl = json
            .decode(response.body)["results"][0]["artworkUrl100"]
            .replaceAll("100", "800")
            .replaceAll("100bb", "800bb");
        return coverImageUrl;
      } else {
        throw Exception('Failed to load data');
      }
    } catch (t, e) {
      print(e);
    }
  }
}
