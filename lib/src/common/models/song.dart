class KonnectRadio {
  String radioName;
  String trackName;
  String song;
  String serverStatus;
  int listeners;
  int maxListeners;
  String artist;

  String album;
  int duration;
  String uri;
  String albumArt;

  KonnectRadio(
      {this.trackName,
      this.artist,
      this.album,
      this.duration,
      this.uri,
      this.albumArt,
      this.song});
  KonnectRadio.fromMap(Map<String, dynamic> json) {
    radioName = json["title"];
    trackName = json["track"]["title"];
    listeners = json["listeners"];
    song = json["song"];
    maxListeners = json["maxlistenrs"];
    artist = json["track"]["artist"];
    album = json["track"]["album"] ?? "Artist";
    duration = json["duration"];
    uri = json["uri"];
    albumArt = json["albumArt"];
  }
}
