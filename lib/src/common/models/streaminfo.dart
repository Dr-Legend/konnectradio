class StreamInfoData {
  String _type;
  List<Data> _data;

  StreamInfoData({String type, List<Data> data}) {
    this._type = type;
    this._data = data;
  }

  String get type => _type;
  set type(String type) => _type = type;
  List<Data> get data => _data;
  set data(List<Data> data) => _data = data;

  StreamInfoData.fromJson(Map<String, dynamic> json) {
    _type = json['type'];
    if (json['data'] != null) {
      _data = new List<Data>();
      json['data'].forEach((v) {
        _data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this._type;
    if (this._data != null) {
      data['data'] = this._data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String _title;
  String _song;
  Track _track;
  String _bitrate;
  String _server;
  String _autodj;
  String _source;
  bool _offline;
  String _summary;
  int _listeners;
  int _maxlisteners;
  int _reseller;
  bool _serverstate;
  int _sourcestate;
  int _sourceconn;
  String _date;
  String _time;
  String _rawmeta;
  String _mountpoint;
  String _tuneinurl;
  String _directtuneinurl;
  String _proxytuneinurl;
  String _tuneinformat;
  String _webplayer;
  String _servertype;
  int _listenertotal;
  String _url;

  Data(
      {String title,
      String song,
      Track track,
      String bitrate,
      String server,
      String autodj,
      String source,
      bool offline,
      String summary,
      int listeners,
      int maxlisteners,
      int reseller,
      bool serverstate,
      int sourcestate,
      int sourceconn,
      String date,
      String time,
      String rawmeta,
      String mountpoint,
      String tuneinurl,
      String directtuneinurl,
      String proxytuneinurl,
      String tuneinformat,
      String webplayer,
      String servertype,
      int listenertotal,
      String url}) {
    this._title = title;
    this._song = song;
    this._track = track;
    this._bitrate = bitrate;
    this._server = server;
    this._autodj = autodj;
    this._source = source;
    this._offline = offline;
    this._summary = summary;
    this._listeners = listeners;
    this._maxlisteners = maxlisteners;
    this._reseller = reseller;
    this._serverstate = serverstate;
    this._sourcestate = sourcestate;
    this._sourceconn = sourceconn;
    this._date = date;
    this._time = time;
    this._rawmeta = rawmeta;
    this._mountpoint = mountpoint;
    this._tuneinurl = tuneinurl;
    this._directtuneinurl = directtuneinurl;
    this._proxytuneinurl = proxytuneinurl;
    this._tuneinformat = tuneinformat;
    this._webplayer = webplayer;
    this._servertype = servertype;
    this._listenertotal = listenertotal;
    this._url = url;
  }

  String get title => _title;
  set title(String title) => _title = title;
  String get song => _song;
  set song(String song) => _song = song;
  Track get track => _track;
  set track(Track track) => _track = track;
  String get bitrate => _bitrate;
  set bitrate(String bitrate) => _bitrate = bitrate;
  String get server => _server;
  set server(String server) => _server = server;
  String get autodj => _autodj;
  set autodj(String autodj) => _autodj = autodj;
  String get source => _source;
  set source(String source) => _source = source;
  bool get offline => _offline;
  set offline(bool offline) => _offline = offline;
  String get summary => _summary;
  set summary(String summary) => _summary = summary;
  int get listeners => _listeners;
  set listeners(int listeners) => _listeners = listeners;
  int get maxlisteners => _maxlisteners;
  set maxlisteners(int maxlisteners) => _maxlisteners = maxlisteners;
  int get reseller => _reseller;
  set reseller(int reseller) => _reseller = reseller;
  bool get serverstate => _serverstate;
  set serverstate(bool serverstate) => _serverstate = serverstate;
  int get sourcestate => _sourcestate;
  set sourcestate(int sourcestate) => _sourcestate = sourcestate;
  int get sourceconn => _sourceconn;
  set sourceconn(int sourceconn) => _sourceconn = sourceconn;
  String get date => _date;
  set date(String date) => _date = date;
  String get time => _time;
  set time(String time) => _time = time;
  String get rawmeta => _rawmeta;
  set rawmeta(String rawmeta) => _rawmeta = rawmeta;
  String get mountpoint => _mountpoint;
  set mountpoint(String mountpoint) => _mountpoint = mountpoint;
  String get tuneinurl => _tuneinurl;
  set tuneinurl(String tuneinurl) => _tuneinurl = tuneinurl;
  String get directtuneinurl => _directtuneinurl;
  set directtuneinurl(String directtuneinurl) =>
      _directtuneinurl = directtuneinurl;
  String get proxytuneinurl => _proxytuneinurl;
  set proxytuneinurl(String proxytuneinurl) => _proxytuneinurl = proxytuneinurl;
  String get tuneinformat => _tuneinformat;
  set tuneinformat(String tuneinformat) => _tuneinformat = tuneinformat;
  String get webplayer => _webplayer;
  set webplayer(String webplayer) => _webplayer = webplayer;
  String get servertype => _servertype;
  set servertype(String servertype) => _servertype = servertype;
  int get listenertotal => _listenertotal;
  set listenertotal(int listenertotal) => _listenertotal = listenertotal;
  String get url => _url;
  set url(String url) => _url = url;

  Data.fromJson(Map<String, dynamic> json) {
    _title = json['title'];
    _song = json['song'];
    _track = json['track'] != null ? new Track.fromJson(json['track']) : null;
    _bitrate = json['bitrate'];
    _server = json['server'];
    _autodj = json['autodj'];
    _source = json['source'];
    _offline = json['offline'];
    _summary = json['summary'];
    _listeners = json['listeners'];
    _maxlisteners = json['maxlisteners'];
    _reseller = json['reseller'];
    _serverstate = json['serverstate'];
    _sourcestate = json['sourcestate'];
    _sourceconn = json['sourceconn'];
    _date = json['date'];
    _time = json['time'];
    _rawmeta = json['rawmeta'];
    _mountpoint = json['mountpoint'];
    _tuneinurl = json['tuneinurl'];
    _directtuneinurl = json['directtuneinurl'];
    _proxytuneinurl = json['proxytuneinurl'];
    _tuneinformat = json['tuneinformat'];
    _webplayer = json['webplayer'];
    _servertype = json['servertype'];
    _listenertotal = json['listenertotal'];
    _url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this._title;
    data['song'] = this._song;
    if (this._track != null) {
      data['track'] = this._track.toJson();
    }
    data['bitrate'] = this._bitrate;
    data['server'] = this._server;
    data['autodj'] = this._autodj;
    data['source'] = this._source;
    data['offline'] = this._offline;
    data['summary'] = this._summary;
    data['listeners'] = this._listeners;
    data['maxlisteners'] = this._maxlisteners;
    data['reseller'] = this._reseller;
    data['serverstate'] = this._serverstate;
    data['sourcestate'] = this._sourcestate;
    data['sourceconn'] = this._sourceconn;
    data['date'] = this._date;
    data['time'] = this._time;
    data['rawmeta'] = this._rawmeta;
    data['mountpoint'] = this._mountpoint;
    data['tuneinurl'] = this._tuneinurl;
    data['directtuneinurl'] = this._directtuneinurl;
    data['proxytuneinurl'] = this._proxytuneinurl;
    data['tuneinformat'] = this._tuneinformat;
    data['webplayer'] = this._webplayer;
    data['servertype'] = this._servertype;
    data['listenertotal'] = this._listenertotal;
    data['url'] = this._url;
    return data;
  }
}

class Track {
  String _artist;
  String _title;
  String _album;
  int _royaltytrackid;
  int _started;
  String _imageurl;

  Track(
      {String artist,
      String title,
      String album,
      int royaltytrackid,
      int started,
      String imageurl}) {
    this._artist = artist;
    this._title = title;
    this._album = album;
    this._royaltytrackid = royaltytrackid;
    this._started = started;
    this._imageurl = imageurl;
  }

  String get artist => _artist;
  set artist(String artist) => _artist = artist;
  String get title => _title;
  set title(String title) => _title = title;
  String get album => _album;
  set album(String album) => _album = album;
  int get royaltytrackid => _royaltytrackid;
  set royaltytrackid(int royaltytrackid) => _royaltytrackid = royaltytrackid;
  int get started => _started;
  set started(int started) => _started = started;
  String get imageurl => _imageurl;
  set imageurl(String imageurl) => _imageurl = imageurl;

  Track.fromJson(Map<String, dynamic> json) {
    _artist = json['artist'];
    _title = json['title'];
    _album = json['album'];
    _royaltytrackid = json['royaltytrackid'];
    _started = json['started'];
    _imageurl = json['imageurl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['artist'] = this._artist;
    data['title'] = this._title;
    data['album'] = this._album;
    data['royaltytrackid'] = this._royaltytrackid;
    data['started'] = this._started;
    data['imageurl'] = this._imageurl;
    return data;
  }
}
