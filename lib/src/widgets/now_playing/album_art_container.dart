import 'package:flutter/material.dart';
import 'package:konnectradio/src/common/models/song.dart';
import 'package:konnectradio/src/common/models/streaminfo.dart';
import 'package:konnectradio/src/utils/kradioApp.dart';

class AlbumArtContainer extends StatelessWidget {
  const AlbumArtContainer({
    Key key,
    @required double radius,
    @required double albumArtSize,
    @required Data currentSong,
  })  : _radius = radius,
        _albumArtSize = albumArtSize,
        _currentSong = currentSong,
        super(key: key);

  final double _radius;
  final double _albumArtSize;
  final Data _currentSong;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(_radius),
          bottomRight: Radius.circular(_radius)),
      child: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: _albumArtSize,
            child: FadeInImage(
              placeholder: AssetImage("assets/images/profile_simple.png"),
              image: NetworkImage(
                _currentSong.track.album,
              ),
              fit: BoxFit.fill,
            ),
          ),
          Opacity(
            opacity: 0.5,
            child: Container(
              width: double.infinity,
              height: _albumArtSize,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  stops: [
                    0.30,
                    0.85,
                  ],
                  colors: [
                    KonnectRadioColors.purple,
//                    KonnectRadioColors.lightGreen
                    Colors.red[400],
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
