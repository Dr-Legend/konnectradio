import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_mailer/flutter_mailer.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:konnectradio/src/common/models/song.dart';
import 'package:konnectradio/src/common/models/streaminfo.dart';
import 'package:konnectradio/src/utils/kradioApp.dart';
import 'package:konnectradio/src/widgets/now_playing/album_art_container.dart';
import 'package:konnectradio/src/widgets/now_playing/empty_album_art_container.dart';
import 'package:shimmer/shimmer.dart';
import 'dart:async';

import 'package:url_launcher/url_launcher.dart';
//import 'package:flutter_appavailability/flutter_appavailability.dart';

typedef void OnError(Exception exception);
enum PlayerState { stopped, playing, paused }

class NowPlayingScreen extends StatefulWidget {
  final Data currentTrack;
  final RmxAudioPlayer rmxAudioPlayer;
  const NowPlayingScreen(
      {Key key, this.currentTrack, @required this.rmxAudioPlayer})
      : super(key: key);
  @override
  _NowPlayingScreenState createState() => _NowPlayingScreenState();
}

class _NowPlayingScreenState extends State<NowPlayingScreen> {
//  AudioPlayer audioPlayer;
//  Duration position;
//  Duration duration;
//  PlayerStates playerState = PlayerStates.stopped;

  //Audioplayers
  String url;
  bool isLocal = false;
  PlayerMode mode = PlayerMode.MEDIA_PLAYER;
  AudioPlayer _audioPlayer;
  AudioPlayerState _audioPlayerState;
  Duration _duration;
  Duration _position;
  PlayerState _playerState = PlayerState.stopped;
  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;
  StreamSubscription _playerErrorSubscription;
  StreamSubscription _playerStateSubscription;
  get _isPlaying => this.widget.rmxAudioPlayer.currentState == 'playing';
  get _isPaused =>
      this.widget.rmxAudioPlayer.currentState == 'paused' ||
      this.widget.rmxAudioPlayer.currentState == 'stopped' ||
      this.widget.rmxAudioPlayer.currentState == 'unknown';
  get _durationText => _duration?.toString()?.split('.')?.first ?? '';
  get _positionText => _position?.toString()?.split('.')?.first ?? '';
  bool dialVisible = true;
  FlutterMailer flutterMailer;
  GlobalKey<ScaffoldState> _scafoldKey = GlobalKey<ScaffoldState>();
  void setDialVisible(bool value) {
    setState(() {
      dialVisible = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _prepare();
    flutterMailer = FlutterMailer();
  }

  @override
  void dispose() {
    super.dispose();
  }

  //Playlist
  _prepare() async {
    await this.widget.rmxAudioPlayer.addAllItems([
      new AudioTrack(
          trackId: 'KonnectRadio',
          album: "Konnect Radio",
          artist: this.widget.currentTrack.track.artist,
          albumArt: this.widget.currentTrack.track.album,
          assetUrl: KonnectRadioConstant.listenStream1,
          title: this.widget.currentTrack.track.title),
    ]);

    await _play();
  }

  _play() async {
    setState(() {});

    await this.widget.rmxAudioPlayer.play();
  }

  _pause() {
    this.widget.rmxAudioPlayer.pause().then((_) {
      print(_);
      setState(() {});
    }).catchError(print);
  }

  @override
  Widget build(BuildContext context) {
    final double _radius = 25.0;
    final double _screenHeight = MediaQuery.of(context).size.height;
    final double _albumArtSize = _screenHeight / 2.1;
    return SafeArea(
      child: Scaffold(
        floatingActionButton: SpeedDial(
          marginRight: 18,
          marginBottom: 20,
          animatedIcon: AnimatedIcons.menu_close,
          animatedIconTheme: IconThemeData(size: 22.0),
          visible: dialVisible,
          closeManually: false,
          curve: Curves.bounceIn,
          overlayColor: Colors.black,
          heroTag: 'speed-dial-hero-tag',
          backgroundColor: Colors.white,
          foregroundColor: KonnectRadioColors.lightGreen,
          elevation: 8.0,
          shape: CircleBorder(),
          children: [
            SpeedDialChild(
                child: Icon(
                  GroovinMaterialIcons.facebook,
                ),
                backgroundColor: Colors.red,
                label: 'Facebook',
                labelStyle: TextStyle(fontSize: 18.0),
                onTap: () async {
//                  AppAvailability.launchApp("fb://profile/718783211555593")
//                      .catchError((e) {
//                    print("App Is Not Installed");
//                    AppAvailability.launchApp(KonnectRadioConstant.facebookUrl);
//                  });
                  await canLaunch("fb://profile/718783211555593")
                      ? launch("fb://profile/718783211555593")
                      : launch(KonnectRadioConstant.facebookUrl);
                }),
            SpeedDialChild(
              child: Icon(GroovinMaterialIcons.twitter),
              backgroundColor: Colors.blue,
              label: 'Twitter',
              labelStyle: TextStyle(fontSize: 18.0),
              onTap: () async {
//                AppAvailability.launchApp("twitter://konnectradio")
//                    .catchError((e) {
//                  print("App Is Not Installed");
//                  AppAvailability.launchApp(KonnectRadioConstant.twitterUrl);
//                });
                await canLaunch("twitter://konnectradio")
                    ? launch("twitter://konnectradio")
                    : launch(KonnectRadioConstant.twitterUrl);
              },
            ),
            SpeedDialChild(
                child: Icon(GroovinMaterialIcons.instagram),
                label: 'Instagram',
                labelStyle: TextStyle(fontSize: 18.0),
                onTap: () async {
//                  AppAvailability.launchApp("instagram://konnectradio")
//                      .catchError((e) {
//                    print("App Is Not Installed");
////
//                    AppAvailability.launchApp(
//                        KonnectRadioConstant.instagramUrl);
//                  });
                  await canLaunch("instagram://konnectradio")
                      ? launch("instagram://konnectradio")
                      : launch(KonnectRadioConstant.instagramUrl);
                }),
            SpeedDialChild(
              child: Icon(GroovinMaterialIcons.email_variant),
              label: 'Mail',
              labelStyle: TextStyle(fontSize: 18.0),
              onTap: () async {
//                AppAvailability.launchApp(KonnectRadioConstant.emailUrl)
//                    .catchError((e) {
//                  debugPrint(e);
//                });
                await canLaunch(KonnectRadioConstant.emailUrl)
                    ? launch(KonnectRadioConstant.emailUrl)
                    : print("Unable to lunch email app");
              },
            ),
          ],
        ),
        drawer: Drawer(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: Stack(
                  children: <Widget>[
                    RotatedBox(
                      child: Container(
                        child: Image.asset(
                          "assets/images/Cover_Patterned_Mint.png",
                          fit: BoxFit.fill,
                          width: MediaQuery.of(context).size.height - 79,
                        ),
                      ),
                      quarterTurns: 1,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          elevation: 0.0,
        ),
        key: _scafoldKey,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              height: _albumArtSize + 50,
              child: Stack(
                children: <Widget>[
                  this.widget.currentTrack?.track.album == null
                      ? EmptyAlbumArtContainer(
                          radius: _radius,
                          albumArtSize: _albumArtSize,
                          iconSize: 200)
                      : AlbumArtContainer(
                          radius: _radius,
                          albumArtSize: _albumArtSize,
                          currentSong: this.widget.currentTrack,
                        ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: 100,
                      width: double.infinity,
                      child: Stack(
                        children: <Widget>[
                          Align(
                              alignment: Alignment.center,
                              child: GestureDetector(
                                onTap: () {
                                  if (_isPlaying) {
                                    print("paused");
                                    _pause();
                                  }
                                  if (_isPaused) {
                                    print("Play");
                                    _play();
                                  }
                                },
                                child: Container(
                                  width: 100,
                                  height: 100,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.15),
                                        blurRadius: 30,
                                        offset: Offset(2, 1.5),
                                      ),
                                    ],
                                  ),
                                  child: Center(
                                    child: AnimatedCrossFade(
                                      duration: Duration(milliseconds: 200),
                                      crossFadeState: _isPlaying
                                          ? CrossFadeState.showFirst
                                          : CrossFadeState.showSecond,
                                      firstChild: Icon(
                                        Icons.pause,
                                        size: 50,
                                        color: KonnectRadioColors.lightGreen,
                                      ),
                                      secondChild: Icon(
                                        Icons.play_arrow,
                                        size: 50,
                                        color: KonnectRadioColors.lightGreen,
                                      ),
                                    ),
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 22.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    flex: 12,
                    child: Container(
                        padding: EdgeInsets.only(top: 30),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Shimmer.fromColors(
                              baseColor: KonnectRadioColors.lightGreen,
                              highlightColor: Colors.teal,
                              period: Duration(seconds: 10),
                              child: Image.asset(
                                "assets/images/konnectlogo.png",
                                color: KonnectRadioColors.lightGreen,
                              ),
                            ),
                            Divider(
                              height: _screenHeight / 22,
                              color: Colors.transparent,
                            ),
                            Text(
                              this.widget.currentTrack.track.title,
                              style: TextStyle(
                                fontSize: 30,
                                color: Colors.black,
                                fontWeight: FontWeight.w700,
                              ),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            Divider(
                              height: 5,
                              color: Colors.transparent,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Flexible(
                                  child: Text(
                                    "ARTIST" +
                                        " • " +
                                        this
                                            .widget
                                            .currentTrack
                                            .track
                                            .artist
                                            .toUpperCase(),
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300,
                                      letterSpacing: 1,
                                    ),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                children: <Widget>[],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
