import 'dart:async';
import 'dart:convert';
import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:konnectradio/src/api/konnect_radio_api.dart';
import 'package:konnectradio/src/common/mixins/lifecycle_mixin.dart';
import 'package:konnectradio/src/common/mixins/loader_mixin.dart';
import 'package:konnectradio/src/common/models/song.dart';
import 'package:konnectradio/src/common/models/streaminfo.dart';
import 'package:konnectradio/src/utils/kradioApp.dart';
import 'package:konnectradio/src/widgets/now_playing/android_player/android_player.dart';
import 'package:konnectradio/src/widgets/now_playing/now_playing_screen.dart';
import 'package:http/http.dart';
import 'package:shimmer/shimmer.dart';

class KonnectRadioPlayer extends StatefulWidget {
  final RmxAudioPlayer rmxAudioPlayer;

  const KonnectRadioPlayer({Key key, @required this.rmxAudioPlayer})
      : super(key: key);
  @override
  _KonnectRadioPlayerState createState() => _KonnectRadioPlayerState();
}

class _KonnectRadioPlayerState extends State<KonnectRadioPlayer>
    with WidgetLifeCycleMixin, LoaderMixin {
  KonnectRadioApi api = KonnectRadioApi();

  double _seeking;
  double _position = 0;

  int _current = 0;
  int _total = 0;
  String _status = 'none';
  Data radio;
  Timer timer;
  bool isTrackChanged;
  Future<Data> getStreamDetails() async {
    try {
      var response =
          await api.getStreamInfo(KonnectRadioConstant.streamInfoAPi);
      var streamInfo = StreamInfoData.fromJson(json.decode(response));
      if (streamInfo.data.isNotEmpty) {
        var kRadio = streamInfo.data.first;
        if (radio?.track?.title != kRadio.track.title || radio == null) {
          setState(() {
            isTrackChanged = true;
          });
        } else {
          setState(() {
            isTrackChanged = false;
          });
        }

        if (isTrackChanged) {
          setState(() {
            radio = kRadio;
          });
          String albumArt = await api.getTrackImage(
              KonnectRadioConstant.getTrackCoverImageURL(query: radio.song));
          setState(() {
            radio.track.album = albumArt;
          });
          print(radio);
        }
      }
    } catch (e) {
      print(e);
    }
    return radio;
  }

  @override
  void initState() {
    super.initState();
    this.widget.rmxAudioPlayer.initialize();
    timer = Timer.periodic(Duration(seconds: 4), (t) {
      getStreamDetails();
      print("API called!");
    });
    this.widget.rmxAudioPlayer.on('status', (eventName, {dynamic args}) {
      print(eventName + (args ?? "").toString());

      if ((args as OnStatusCallbackData).value != null) {
        if (mounted) {
          setState(() {
            if ((args as OnStatusCallbackData).value['currentPosition'] !=
                null) {
              _current = (args as OnStatusCallbackData)
                  .value['currentPosition']
                  .toInt();
              _total = (((args as OnStatusCallbackData).value['duration']) ?? 0)
                  .toInt();
              _status = (args as OnStatusCallbackData).value['status'];

              if (_current > 0 && _total > 0) {
                _position = _current / _total;
              } else if (!this.widget.rmxAudioPlayer.isLoading &&
                  !this.widget.rmxAudioPlayer.isSeeking) {
                _position = 0;
              }

              if (_seeking != null &&
                  !this.widget.rmxAudioPlayer.isSeeking &&
                  !this.widget.rmxAudioPlayer.isLoading) {
                _seeking = null;
              }
            }
          });
        }
      }
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    if (timer.isActive) {
      timer.cancel();
    }
    super.dispose();
  }

//  radio == null
//  ? Center(
//  child: CircularProgressIndicator(),
//  )
//      : NowPlayingScreen(
//  currentTrack: radio,
//  )

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: OfflineBuilder(
      connectivityBuilder: (
        BuildContext context,
        ConnectivityResult connectivity,
        Widget child,
      ) {
        final bool connected = connectivity != ConnectivityResult.none;
        if (!connected) {
          if (timer.isActive) {
            timer.cancel();
          }
        }
        return SafeArea(
          child: Stack(
//          fit: StackFit.expand,
            children: [
              child,
              !connected
                  ? Positioned(
                      height: 24.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        color:
                            connected ? Color(0xFF00EE44) : Color(0xFFEE4400),
                        child: Center(
                          child: Text(
                            "${connected ? 'ONLINE' : 'You are Offline'}",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    )
                  : Container(),
//            Center(
//              child: new Text(
//                'Yay!',
//              ),
//            ),
            ],
          ),
        );
      },
      child: radio == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : NowPlayingScreen(
              currentTrack: radio,
              rmxAudioPlayer: this.widget.rmxAudioPlayer,
            ),
    ));
  }
}

class TestScreen extends StatefulWidget {
  @override
  _TestScreenState createState() => _TestScreenState();
}

class _TestScreenState extends State<TestScreen> {
  Future<void> audioStart() async {
    print('Audio Start OK');
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
